#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;
//int opcion[0], tiempoServicio[0], sexo[0], cargo[0] ;
int total=0, numeroEmpleados, confirmar, opcion,
    //indicadores por Cargo
    cantidadCargo[2], porcentajeCargo[2], promedioCargo[2], sumaTotalSalarioCargo[2];
bool noSalir = true, valido = false;
//string datosEmpleados[];

float SMMLV = 644400;

/*
 * 
 */
int main(int argc, char** argv) {

    system("color 8B");
    cout << "| ___ \\ | | | ___ \\ / __  \\|  _  |/  | |  ___| /  __ \\ _     _   \n"
            "| |_/ / | | | |_/ / `' / /'| |/' |`| | |___ \\  | /  \\/| |_ _| |_   \n"
            "|    /| | | |  __/    / /  |  /| | | |     \\ \\ | |  |_   _|_   _|  \n"
            "| |\\ \\| |_| | |     ./ /___\\ |_/ /_| |_/\\__/ / | \\__/\\|_|   |_|\n"
            "\\_| \\_|\\___/\\_|     \\_____/ \\___/ \\___/\\____/   \\____/      \n"
            "__________"
            "Presione enter para continuar...";
    cin.get();
    if (system("CLS")) system("clear");
    //Numero empleados de la empresa e inicialización de arrays
    cout<<"Digite la cantidad de empleados: "<<endl;
    cin>>numeroEmpleados;
    int tiempoServicio[numeroEmpleados], sexo[numeroEmpleados], cargo[numeroEmpleados];
    float salario[numeroEmpleados];

    while (noSalir) {
        //Limpiar pantalla de la terminal
        if (system("CLS")) system("clear");
        //Limpiar variables de indicadores
        cantidadCargo[0] = cantidadCargo[1] = cantidadCargo[2] = 0;
        porcentajeCargo[0] = porcentajeCargo[1] = porcentajeCargo[2] = 0;
        promedioCargo[0] = promedioCargo[1] = promedioCargo[2] = 0;
        sumaTotalSalarioCargo[0] = sumaTotalSalarioCargo[1] = sumaTotalSalarioCargo[2] = 0;
        //Presentar menu
        cout<< "Digite la opción que desea: \n"
                "\t 1. Nuevo empleado \n"
                "\t 2. Borrar empleados \n"
                "\t 3. Reporte por cargo \n"
                "\t 4. Reporte por operarios hombres que ganen menos de 2 SMMLV \n"
                "\t 5. Reporte por administradoras que ganan mas de 4 SMMLV \n"
                "\t 6. Salir \n"
                ;
        cin>> opcion;
        //Siempre asumimos que se va a ingresar un valor valido hasta validarlo como tal
        valido = true;
        if (system("CLS")) system("clear");
        switch (opcion) {
            case 1: //Nuevo empleado
                //Validamos que no agreguemos mas de el total de empleados
                if(total == numeroEmpleados){
                    cout << "No se pueden agregar mas empleados: " << numeroEmpleados;
                    break;
                }
                //Pregunta tiempo servicio
                do {
                    cout << "Digite el tiempo de servicio del empleado (max 20 años):" << endl;
                    cin >> tiempoServicio[total];
                    // Validamos el valor digitado
                    if((tiempoServicio[total] < 1) || (tiempoServicio[total] > 20)){
                        valido = false;
                        cout << "Valor invalido" << endl;
                    }else{
                        valido=true;
                    }
                } while (!valido);
                //Pregunta sexo
                do {
                    cout << "Digite un numero que corresponda al sexo (1=hombre, 2=mujer):" << endl;
                    cin >> sexo[total];
                    // Validamos el valor digitado
                    if(!(sexo[total] == 1 || sexo[total] == 2)){
                        valido = false;
                        cout << "Valor invalido" << endl;
                    }else{
                        valido=true;
                    }
                } while (!valido);
                //Pregunta salario mensual
                do {
                    cout << "Digite salario mensual "
                            "(min=" << SMMLV <<"):" << endl;
                    cin >> salario[total];
                    // Validamos el valor digitado
                    if(salario[total] < SMMLV){
                        valido = false;
                        cout << "Valor invalido" << endl;
                    }else{
                        valido=true;
                    }
                } while (!valido);
                //Pregunta cargo
                do {
                    cout << "Escoja el cargo "
                            "(1=operador, 2=supervisor, 3=administrador):" << endl;
                    cin >> cargo[total];
                    // Validamos el valor digitado
                    if(!(cargo[total] == 1 || cargo[total] == 2 || cargo[total] == 3)){
                        valido = false;
                        cout << "Valor invalido" << endl;
                    }else{
                        valido=true;
                    }
                } while (!valido);
                total++;
                break; 
            case 2: //Borrar empleados
                //Preguntar si estamos seguros
                cout << "¿Seguro que desea borrar la base de empleados? (1=Si, 0=No)";
                cin >> confirmar;
                if(confirmar == 1){
                    //tiempoServicio = sexo = cargo = int[numeroEmpleados];
                    //salario[] = {};
                    total = 0;
                }
                cout << "Base de datos borrada";
                break; 
            case 3: //Reporte por cargo
                //Calculo de Indicadores
                //Cantidad
                for(int i=0;i<total;i++){
                    int cargoC = cargo[i]-1;
                    cantidadCargo[cargoC]++; //El cargo es +1 de la posicion del array
                    sumaTotalSalarioCargo[cargoC] += salario[i];
                }
                //Porcentaje y promedio
                for(int i=0;i<=2;i++){
                    porcentajeCargo[i] = cantidadCargo[i]*100 / total;
                    promedioCargo[i] = sumaTotalSalarioCargo[i] / cantidadCargo[i];
                }
                //Mostrar cabecera de reporte
                cout << "Reporte por cargo"
                    "-----------------------------------------------------\n"
                    "\t Cantidad |\t Porcentaje |\t Promedio |\t Cargo \n";
                for(int i=0;i<=2;i++){
                    //Imprimir filas de reporte
                    cout<< "\t" 
                        << cantidadCargo[i] << "\t" 
                        << porcentajeCargo[i] << "\t" 
                        << promedioCargo[i] << "\t"
                        << i<< "\t"
                        << endl;
                }
                break; 
            case 4: //Reporte por operarios
                //Calculo de Indicadores
                //Cantidad
                for(int i=0;i<total;i++){
                    //Operarios hombres
                    if(cargo[i] == 1 && sexo[i] == 1 && salario[i] < 2*SMMLV){
                        cantidadCargo[0]++; //El cargo es +1 de la posicion del array
                    }
                }
                porcentajeCargo[0] = cantidadCargo[0]*100 / total;
                
                //Mostrar cabecera de reporte
                cout<< "Reporte por cargo"
                    "-----------------------------------------------------\n"
                    "\t Cantidad |\t Porcentaje \n";
                cout<< "\t" 
                    << cantidadCargo[0] << "\t" 
                    << porcentajeCargo[0] << "\t"
                    << endl;
                break; 
            case 5: //Reporte por administradoras
                
                //Calculo de Indicadores
                //Cantidad
                for(int i=0;i<total;i++){
                    //Administradoras mujeres
                    if(cargo[i] == 3 && sexo[i] == 2 && salario[i] > 4*SMMLV){
                        cantidadCargo[0]++; //El cargo es +1 de la posicion del array
                    }
                }
                porcentajeCargo[0] = cantidadCargo[0]*100 / total;
                
                //Mostrar cabecera de reporte
                cout<< "Reporte por cargo"
                    "-----------------------------------------------------\n"
                    "\t Cantidad |\t Porcentaje \n";
                cout<< "\t" 
                    << cantidadCargo[0] << "\t" 
                    << porcentajeCargo[0] << "\t"
                    << endl;

                break; 
            case 6: //Salir
                noSalir = false;
                break;
            default: //Optional
                break;
            
        }
        
        cout<< "Presione cualquier tecla para continuar";
        cin.get();
    }
    
    return 0;
}

